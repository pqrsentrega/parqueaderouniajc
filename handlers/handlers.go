package handlers

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository/usuario"
	veh "bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository/vehiculo"
	"encoding/json"
	"net/http"
	"time"

	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/token"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/jwt"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository"
)

const tipo = "application/json"

// Route ...
type Route struct {
	Repo  repository.UserRepository
	Repo2 repository.VehiculoRepository
}

// NewUserHandler ...
func NewUserHandler(db *bd.Data) *Route {
	return &Route{
		Repo:  usuario.NewUserRepo(db),
		Repo2: veh.NewVehiculoRepo(db),
	}
}

/*Login ruta de funcion para el login del usuario*/
func (ur *Route) Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", tipo)

	var t models.Usuario

	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		http.Error(w, "Usuario y/o Contraseña inválidos "+err.Error(), 400)
		return
	}
	if len(t.Email) == 0 {
		http.Error(w, "El email del usuario es requerido ", 400)
		return
	}
	documento, existe := ur.Repo.Login(t.Email, t.Password)
	if existe == false {
		http.Error(w, "Usuario y/o Contraseña inválidos ", 400)
		return
	}

	jwtKey, err := jwt.GenerateJWT(documento)
	if err != nil {
		http.Error(w, "Ocurrió un error al intentar general el Token correspondiente "+err.Error(), 400)
		return
	}

	resp := models.RespuestaLogin{
		Token: jwtKey,
	}

	w.Header().Set("Content-Type", tipo)
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(resp)

	expirationTime := time.Now().Add(24 * time.Hour)
	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   jwtKey,
		Expires: expirationTime,
	})
}

/*Registro es la función para crear en la BD el registro de usuario */
func (ur *Route) Registro(w http.ResponseWriter, r *http.Request) {

	var t models.Usuario

	err := json.NewDecoder(r.Body).Decode(&t)
	print(t.Apellido)
	if err != nil {
		http.Error(w, "Error en los datos recibidos"+err.Error(), 400)
		return
	}
	if len(t.Email) == 0 {
		http.Error(w, "El Email de usuario es requerido", 400)
		return
	}
	if len(t.Password) < 6 {
		http.Error(w, "Debe colocar una contraseña minima de 6 caracteres", 400)
		return
	}

	_, encontrado, _ := usuario.CheckYaExisteUsuario(t.Email)
	if encontrado == true {
		http.Error(w, "Ya existe un usuario registrado con ese email", 400)
		return
	}

	_, status, err := ur.Repo.InsertoRegistro(t)
	if err != nil {
		http.Error(w, "Ocurrio un error al intentar realizar el registro de usuario"+err.Error(), 400)
		return
	} else {

		print("Ingreso en insert")
	}

	if status == false {
		http.Error(w, "No se ha logrado insertar el registro del usuario", 400)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Se creo el registro correctamente"))
}

/*CambiarEstadoUsuario editar datos del usuario*/
func (ur *Route) CambiarEstadoUsuario(w http.ResponseWriter, r *http.Request) {
	var t models.Usuario

	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		http.Error(w, "Datos Incorrectos "+err.Error(), 400)
		print("Datos Incorrectos")
		return
	}

	var status bool

	status, err = ur.Repo.EditarUsuario(t, token.IDUsuario)
	if err != nil {
		http.Error(w, "Ocurrión un error al intentar modificar el registro. Reintente nuevamente "+err.Error(), 400)
		print("error err")
		return
	}

	if status == false {
		http.Error(w, "No se ha logrado modificar el registro del usuario ", 400)
		print("error estatus")
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Se cambio el estado correctamente"))
}

/*ConsultarUsuario route para consultar usuario por ID*/
func (ur *Route) ConsultarUsuario(w http.ResponseWriter, r *http.Request) {

	usuario, err := ur.Repo.ConsultarUsuarioDB(token.IDUsuario)
	if err != nil {
		http.Error(w, "Ocurrio un error al intentar buscar el registro"+err.Error(), 400)
		return
	}

	w.Header().Set("context-type", tipo)
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(usuario)
}

/*EditarUsuario editar datos del usuario*/
func (ur *Route) EditarUsuario(w http.ResponseWriter, r *http.Request) {
	var t models.Usuario

	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		http.Error(w, "Datos Incorrectos "+err.Error(), 400)
		print("Datos Incorrectos")
		return
	}

	var status bool

	status, err = ur.Repo.EditarUsuario(t, token.IDUsuario)
	if err != nil {
		http.Error(w, "Ocurrión un error al intentar modificar el registro. Reintente nuevamente "+err.Error(), 400)
		print("error err")
		return
	}

	if status == false {
		http.Error(w, "No se ha logrado modificar el registro del usuario ", 400)
		print("error estatus")
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Se edito el registro correctamente"))
}

/*ConsultarVehiculos route para vehiculos  por ID*/
func (vr *Route) ConsultarVehiculos(w http.ResponseWriter, r *http.Request) {
	ID := r.URL.Query().Get("id")
	if len(ID) < 1 {
		http.Error(w, "Debe enviar el identificador del vehiculo", http.StatusBadRequest)
		return
	}
	vehiculo, err := vr.Repo2.ConsultarVehiculo(ID)
	if err != nil {
		http.Error(w, "Ocurrio un error al intentar buscar el registro"+err.Error(), 400)
		return
	}

	w.Header().Set("context-type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(vehiculo)
}

/*EditarVehiculo editar datos del vehiculo*/
func (vr *Route) EditarVehiculo(w http.ResponseWriter, r *http.Request) {
	var t models.Vehiculo

	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		http.Error(w, "Datos Incorrectos "+err.Error(), 400)
		print("Datos Incorrectos")
		return
	}

	var status bool

	status, err = vr.Repo2.EditarVehiculo(t, t.ID)
	if err != nil {
		http.Error(w, "Ocurrión un error al intentar modificar el registro. Reintente nuevamente "+err.Error(), 400)
		print("error", err)
		return
	}

	if status == false {
		http.Error(w, "No se ha logrado modificar el registro del vehiculo ", 400)
		print("error estatus")
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Se edito el registro correctamente"))
}

/*RegistroVehiculo es la función para crear en la BD el registro de vehiculos */
func (vr *Route) RegistroVehiculo(w http.ResponseWriter, r *http.Request) {

	var t models.Vehiculo

	err := json.NewDecoder(r.Body).Decode(&t)
	print(t.Placa)
	if err != nil {
		http.Error(w, "Error en los datos recibidos"+err.Error(), 400)
		return
	}
	if len(t.Modelo) == 0 {
		http.Error(w, "El Modelo del vehiculo es requerido", 400)
		return
	}
	if len(t.Color) == 0 {
		http.Error(w, "Debe colocar un c", 400)
		return
	}

	_, encontrado, _ := usuario.CheckYaExisteVehiculo(t.Placa)
	if encontrado == true {
		http.Error(w, "Ya existe un vehiculo registrado con esa placa", 400)
		return
	}

	_, status, err := vr.Repo2.InsertoRegistroVehiculo(t)
	if err != nil {
		http.Error(w, "Ocurrio un error al intentar realizar el registro de usuario"+err.Error(), 400)
		return
	} else {
		print("Ingreso en insert")
	}

	if status == false {
		http.Error(w, "No se ha logrado insertar el registro del usuario", 400)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Se creo el vehiculo"))
}
